# Upgrade Terraform

Téléchargement ou mise à jour de Terraform vers la version "latest"
Nécessite les commandes curl, unzip et jq. Affiche un message d'erreur si les commandes ne sont pas dispo.

*Initial fork from* https://gist.github.com/josh-padnick/3fa21bff2b8054a7770f