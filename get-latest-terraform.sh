#!/bin/bash

_needed_commands="curl jq cut wget unzip" ;

checkrequirements () {
        command -v command >/dev/null 2>&1 || {
                echo "WARNING> \"command\" not found. Check requirements skipped !"
                return 1 ;
        }
        for requirement in ${_needed_commands} ; do
                echo -n "Checking if command \"$requirement\" exists ... " ;
                command -v ${requirement} > /dev/null && {
                        echo "ok" ;
                        continue ;
                } || {
                        echo "required but not found !" ;
                        _return=1 ;
                }
                done
        [ -z "${_return}" ] || {
                echo "ERR > Requirement missing." >&2 ;
                exit 1 ;
        }
}

checkrequirements

LATEST_RELEASE=$(curl https://checkpoint-api.hashicorp.com/v1/check/terraform | jq --raw-output '.current_version' | cut -c 1-)
if [[ ! -e ${LATEST_RELEASE} ]]; then
   echo "Installing Terraform ${LATEST_RELEASE}..."
   rm terraform-*
   rm terraform
   wget https://releases.hashicorp.com/terraform/${LATEST_RELEASE}/terraform_${LATEST_RELEASE}_linux_amd64.zip
   unzip terraform_${LATEST_RELEASE}_linux_amd64.zip
   rm terraform_${LATEST_RELEASE}_linux_amd64.zip
   #touch ${LATEST_RELEASE}
   rm .wget-hsts
else
   echo "Latest Terraform already installed."
fi

